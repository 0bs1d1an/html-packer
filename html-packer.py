#!/usr/bin/python
import base64
from os.path import basename
from sys import argv

filename = argv[1]
with open(filename, 'rb') as f:
    data = f.read()

output = '''<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
<script type="text/javascript">

// embedding file as a base64 string
var b64Data = '%s';

// converting base64 string to byteArray
var byteCharacters = atob(b64Data);
var byteNumbers = new Array(byteCharacters.length);
for (var i = 0; i < byteCharacters.length; i++) {
    byteNumbers[i] = byteCharacters.charCodeAt(i);
}
var byteArray = new Uint8Array(byteNumbers);

if (window.navigator.msSaveBlob) {
    // converting byteArray to downloadable blob
    var blob = new Blob([byteArray], {type: "application/octet-stream"});

    // downloading blob, with proper file name
    window.navigator.msSaveBlob(blob, '%s')
} else {
    // converting byteArray to downloadable blob
    var saveByteArray = (function () {
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.style = "display: none";
        return function (data, name) {
            var blob = new File(data, {type: "application/octet-stream"}),
                url = window.URL.createObjectURL(blob);
            a.href = url;
            a.download = name;
            a.click();
            window.URL.revokeObjectURL(url);
        };
    }());

    // downloading blob, with proper file name
    saveByteArray([byteArray], '%s');
}

</script>
</body>
</html>
'''

with open(filename + '.html', 'w') as f:
    f.write(output % (base64.b64encode(data).decode(), basename(filename), basename(filename)))
