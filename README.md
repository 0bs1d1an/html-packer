# html-packer

Packing files inside an HTML document. The browser prompts the user to download the embedded file. Useful to bypass some basic email filters.

## Example

`./html-packyer.py [file]`

## Acknowledgements

Original Internet Explorer method by [Arris Huijgen](https://github.com/bitsadmin)
